package br.com.trabalhoPM.maven.controller;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.apache.commons.validator.routines.EmailValidator;
import org.eclipse.jetty.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonProperty;

import br.com.trabalhoPM.maven.controller.dto.CartaoDeCreditoDTO;
import br.com.trabalhoPM.maven.controller.dto.ResponseDTO;
import br.com.trabalhoPM.maven.modelo.Ciclista;
import br.com.trabalhoPM.maven.modelo.Ciclista.Passaporte;
import io.javalin.http.Context;

public class CiclistaController {	
	
	private static List<Ciclista> listaCiclista = new ArrayList<>();
	
	/*public static void getCartaoDeCredito(Context ctx) {
		String idCiclistaString = ctx.pathParam("idCiclista");
		UUID idCiclista;
		try {
			idCiclista = UUID.fromString(idCiclistaString);
		} catch(IllegalArgumentException e) {
			ctx.status(HttpStatus.UNPROCESSABLE_ENTITY_422);
			ctx.json(new ResponseDTO(idCiclistaString, "422","Dados Inválidos"));
			return;
		}
		for (Ciclista ciclista : listaCiclista) {
			if(ciclista.getIdCiclista().equals(idCiclista)) {
				ctx.json(new CartaoDeCreditoDTO(ciclista.getIdCiclista()));
				return;
			}
		}
		ctx.status(HttpStatus.NOT_FOUND_404);
		ctx.json(new ResponseDTO(idCiclistaString, "404","Não encontrado"));
	}*/
	
	public static void getEmails(Context ctx) {
		String emailParam = ctx.pathParam("email");
		if(emailParam == null) {
			ctx.status(HttpStatus.BAD_REQUEST_400);
			ctx.json(new ResponseDTO(null, "400","Email não enviado como parâmetro"));
		}
		if(!EmailValidator.getInstance().isValid(emailParam)) {
			ctx.status(HttpStatus.UNPROCESSABLE_ENTITY_422);
			ctx.json(new ResponseDTO(null, "422","Dados Inválidos"));
		}
		for (Ciclista ciclista : listaCiclista) {
			if(ciclista.getEmail().equals(emailParam)) {
				ctx.result("true");
				return;
			}
		}
		ctx.result("false");
	}
	
	public static void cadastrarCiclista(Context ctx){
		CadastrarCiclistaEMeioDePagamentoDTO cadastrarCiclistaDTO = ctx.bodyAsClass(CadastrarCiclistaEMeioDePagamentoDTO.class);
		
		Ciclista obterCiclista = cadastrarCiclistaDTO.ciclista.obterCiclista();
		System.out.println(obterCiclista);
		
		
		listaCiclista.add(obterCiclista);
		obterCiclista.setIdCiclista(UUID.randomUUID());
		ctx.json(new CiclistaDTO(obterCiclista));
	}
	
	public static void getCiclista(Context ctx) {
		String idCiclistaString = ctx.pathParam("idCiclista");
		UUID idCiclista;
		try {
			idCiclista = UUID.fromString(idCiclistaString);
		} catch(IllegalArgumentException e) {
			ctx.status(HttpStatus.UNPROCESSABLE_ENTITY_422);
			ctx.json(new ResponseDTO(idCiclistaString, "422","Dados Inválidos"));
			return;
		}
		for (Ciclista ciclista : listaCiclista) {
			if(ciclista.getIdCiclista().equals(idCiclista)) {
				ctx.json(new CiclistaDTO(ciclista));
				return;
			}
		}
		ctx.status(HttpStatus.NOT_FOUND_404);
		ctx.json(new ResponseDTO(idCiclistaString, "404","Não encontrado"));
	}
	
	public static void putCiclista(Context ctx) {
		Ciclista novoCiclista = ctx.bodyAsClass(CiclistaDTO.class).obterCiclista();
		String idCiclistaString = ctx.pathParam("idCiclista");
		UUID idCiclista = UUID.fromString(idCiclistaString);
		novoCiclista.setIdCiclista(idCiclista);
		
		for (Ciclista ciclista : listaCiclista) {
			if(ciclista.getIdCiclista().equals(idCiclista)) {
				listaCiclista.set(listaCiclista.indexOf(ciclista), novoCiclista);
				ctx.json(new CiclistaDTO(novoCiclista));
				return;
			}
		}
	}
	
	public static class CadastrarCiclistaEMeioDePagamentoDTO{
		@JsonProperty
		CiclistaDTO ciclista;
		@JsonProperty
		MeioDePagamentoDTO meioDePagamento;
	}
	
	public static class CiclistaDTO{
		@JsonProperty
		public String id, nome, nascimento, cpf, nacionalidade, email, senha;
		@JsonProperty
		public PassaporteDTO passaporte;
		
		public CiclistaDTO() {
			
		}
		
		public CiclistaDTO(Ciclista ciclista) {
			if(ciclista.getIdCiclista() != null) {				
				this.id = ciclista.getIdCiclista().toString();
			}
			this.nome = ciclista.getNome();
			this.nascimento = ciclista.getNascimento().toString();
			this.cpf = ciclista.getCpf();
			this.nacionalidade = ciclista.getNacionalidade();
			this.email = ciclista.getEmail();
			this.senha = ciclista.getSenha();
			if(ciclista.getPassaporte() != null) {				
				this.passaporte = new PassaporteDTO(ciclista.getPassaporte());
			}
		}
		
		public static class PassaporteDTO{
			@JsonProperty
			public String numero, validade, pais;
			
			public PassaporteDTO() {
				
			}
			
			public PassaporteDTO(Passaporte passaporte) {
				this.numero = passaporte.getNumero();
				this.validade = passaporte.getValidade().toString();
				this.pais = passaporte.getPais();
			}
			
			public Passaporte obterPassaporte() {
				Passaporte passaporte = new Passaporte();
				
				LocalDate validade = LocalDate.parse(this.validade);
				
				passaporte.setNumero(this.numero);
				passaporte.setValidade(validade);
				passaporte.setPais(this.pais);
				
				return passaporte;
			}
		}
		
		public Ciclista obterCiclista() {
			Ciclista ciclista = new Ciclista();
			
			LocalDate nascimento = LocalDate.parse(this.nascimento);
			
			if(this.id != null) {
				ciclista.setIdCiclista(UUID.fromString(this.id));
			}
			ciclista.setNome(this.nome);
			ciclista.setNascimento(nascimento);
			ciclista.setCpf(this.cpf);
			ciclista.setNacionalidade(this.nacionalidade);
			ciclista.setEmail(this.email);
			ciclista.setSenha(this.senha);
			if(this.passaporte != null) {
				ciclista.setPassaporte(this.passaporte.obterPassaporte());
			}
			
			return ciclista;
		}
	}
	
	public static class MeioDePagamentoDTO{
		@JsonProperty
		String nomeTitular, numero, validade, cvv;

		public String getNomeTitular() {
			return nomeTitular;
		}

		public void setNomeTitular(String nomeTitular) {
			this.nomeTitular = nomeTitular;
		}

		public String getNumero() {
			return numero;
		}

		public void setNumero(String numero) {
			this.numero = numero;
		}

		public String getValidade() {
			return validade;
		}

		public void setValidade(String validade) {
			this.validade = validade;
		}

		public String getCvv() {
			return cvv;
		}

		public void setCvv(String cvv) {
			this.cvv = cvv;
		}
	}
}
