package br.com.trabalhoPM.maven.controller;

import io.javalin.plugin.json.JavalinJson;
import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;

import java.util.Date;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.javalin.http.Context;
import br.com.trabalhoPM.maven.controller.CiclistaController.CiclistaDTO;
import br.com.trabalhoPM.maven.modelo.Aluguel;

public class AluguelController {
	
	public static void aluguel(Context ctx) {
		AluguelDTO aluguelDTO = new AluguelDTO();
		
		Aluguel obterAluguel = aluguelDTO.obterAluguel();
		System.out.println(obterAluguel);
		
		Date horaInicio = new Date();
		
		obterAluguel.setBicicleta(UUID.randomUUID());
		obterAluguel.setTrancaInicio(UUID.randomUUID());
		obterAluguel.setHoraInicio(horaInicio);
		criarCobranca();
		ctx.json(new AluguelDTO(obterAluguel));
	}

	private static void criarCobranca() {
		CriarCobrancaDTO criarCobrancaDTO = new CriarCobrancaDTO(5.0f, "f66354a4-03e6-4003-a5fc-692841cdcdd4");
		HttpResponse<JsonNode> httpResponse = Unirest.post("https://pm-g4-brenofil.herokuapp.com/cobranca/").body(criarCobrancaDTO).asJson();
		if(httpResponse.getStatus() == 200) {
			System.out.println("Cobrança solicitada");
		} else {
			GetErroCobrancaDTO fromJson = JavalinJson.fromJson(httpResponse.getBody().toString(), GetErroCobrancaDTO.class);
			System.out.println(fromJson.mensagem);
		}
	}
	
	public static void main(String[] args) {
		criarCobranca();
	}
	
	public static class AluguelDTO{
		@JsonProperty
		public String bicicleta, trancaFim, cobranca, ciclista, trancaInicio;
		@JsonProperty
		public Date horaInicio, horaFim;
		
		public AluguelDTO() {
			
		}
		
		public AluguelDTO(Aluguel aluguel) {
			this.bicicleta = aluguel.getBicicleta().toString();
			this.trancaFim = aluguel.getTrancaFim().toString();
			this.cobranca = aluguel.getCobranca().toString();
			this.ciclista = aluguel.getCiclista().toString();
			this.trancaInicio = aluguel.getTrancaInicio().toString();
			this.horaInicio = aluguel.getHoraInicio();
			this.horaFim = aluguel.getHoraFim();
		}
		
		public Aluguel obterAluguel() {
			Aluguel aluguel = new Aluguel();
			
			Date horaFim = new Date();
			UUID trancaFim = UUID.fromString(this.trancaFim);
			
			aluguel.setBicicleta(UUID.fromString(this.bicicleta));
			aluguel.setHoraInicio(this.horaInicio);
			aluguel.setTrancaFim(trancaFim);
			aluguel.setHoraFim(horaFim);
			aluguel.setCobranca(UUID.fromString(this.cobranca));
			aluguel.setCiclista(UUID.fromString(this.ciclista));
			aluguel.setTrancaInicio(UUID.fromString(this.trancaInicio));
			
			return aluguel;
		}
	}

	private static class CriarCobrancaDTO{
		float valor;
		String ciclista;
		
		CriarCobrancaDTO(){
			
		}
		
		CriarCobrancaDTO(float valor, String ciclista){
			this.valor = valor;
			this.ciclista = ciclista;
		}
	}
	
	public static class GetErroCobrancaDTO{
		String id, codigo, mensagem;

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getCodigo() {
			return codigo;
		}

		public void setCodigo(String codigo) {
			this.codigo = codigo;
		}

		public String getMensagem() {
			return mensagem;
		}

		public void setMensagem(String mensagem) {
			this.mensagem = mensagem;
		}
	}
	
}
