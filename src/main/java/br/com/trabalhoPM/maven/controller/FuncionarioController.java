package br.com.trabalhoPM.maven.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.eclipse.jetty.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonProperty;

import br.com.trabalhoPM.maven.controller.dto.ResponseDTO;
import br.com.trabalhoPM.maven.modelo.Funcionario;
import io.javalin.http.Context;

public class FuncionarioController {

	private static List<Funcionario> listaFuncionario = new ArrayList<>();
	
	public static void deleteFuncionario(Context ctx) {
		String idFuncionarioString = ctx.pathParam("idFuncionario");
		UUID idFuncionario = UUID.fromString(idFuncionarioString);
		
		for (Funcionario funcionario : listaFuncionario) {
			if(funcionario.getIdFuncionario().equals(idFuncionario)) {
				listaFuncionario.remove(funcionario);
				return;
			}
		}
		ctx.status(HttpStatus.NOT_FOUND_404);
		ctx.json(new ResponseDTO(idFuncionarioString, "404","Não encontrado"));
	}
	
	public static void putFuncionario(Context ctx) {
		Funcionario novoFuncionario = ctx.bodyAsClass(FuncionarioDTO.class).obterFuncionario();
		String idFuncionarioString = ctx.pathParam("idFuncionario");
		UUID idFuncionario = UUID.fromString(idFuncionarioString);
		novoFuncionario.setIdFuncionario(idFuncionario);
		
		for (Funcionario funcionario : listaFuncionario) {
			if(funcionario.getIdFuncionario().equals(idFuncionario)) {
				listaFuncionario.set(listaFuncionario.indexOf(funcionario), novoFuncionario);
				ctx.json(new FuncionarioDTO(novoFuncionario));
				return;
			}
		}
	}
	
	public static void getAllFuncionarios(Context ctx) {
		List<FuncionarioDTO> listaFuncionarioDTO = new ArrayList<>(listaFuncionario.size());
		for (Funcionario funcionario : listaFuncionario) {
			FuncionarioDTO funcionarioDTO = new FuncionarioDTO(funcionario);
			listaFuncionarioDTO.add(funcionarioDTO);
		}
		ctx.json(listaFuncionarioDTO);
	}

	public static void cadastrarFuncionario(Context ctx) {
		FuncionarioDTO cadastrarFuncionarioDTO = ctx.bodyAsClass(FuncionarioDTO.class);

		Funcionario obterFuncionario = cadastrarFuncionarioDTO.obterFuncionario();
		System.out.println(obterFuncionario);

		listaFuncionario.add(obterFuncionario);
		obterFuncionario.setIdFuncionario(UUID.randomUUID());
		ctx.json(new FuncionarioDTO(obterFuncionario));
	}

	public static void getFuncionario(Context ctx) {
		String idFuncionarioString = ctx.pathParam("idFuncionario");
		UUID idFuncionario;
		try {
			idFuncionario = UUID.fromString(idFuncionarioString);
		} catch(IllegalArgumentException e) {
			ctx.status(HttpStatus.UNPROCESSABLE_ENTITY_422);
			ctx.json(new ResponseDTO(idFuncionarioString, "422","Dados Inválidos"));
			return;
		}
		for (Funcionario funcionario : listaFuncionario) {
			if(funcionario.getIdFuncionario().equals(idFuncionario)) {
				ctx.json(new FuncionarioDTO(funcionario));
				return;
			}
		}
		ctx.status(HttpStatus.NOT_FOUND_404);
		ctx.json(new ResponseDTO(idFuncionarioString, "404","Não encontrado"));
	}

	public static class FuncionarioDTO {
		@JsonProperty
		public String id, matricula, senha, email, nome, funcao, cpf;
		@JsonProperty
		public int idade;

		public FuncionarioDTO() {

		}

		public FuncionarioDTO(Funcionario funcionario) {
			this.id = funcionario.getIdFuncionario().toString();
			this.matricula = funcionario.getMatricula();
			this.senha = funcionario.getSenha();
			this.email = funcionario.getEmail();
			this.nome = funcionario.getNome();
			this.funcao = funcionario.getFuncao();
			this.cpf = funcionario.getCpf();
			this.idade = funcionario.getIdade();
		}

		public Funcionario obterFuncionario() {
			Funcionario funcionario = new Funcionario();

			if (this.id != null) {
				funcionario.setIdFuncionario(UUID.fromString(this.id));
			}
			funcionario.setMatricula(this.matricula);
			funcionario.setSenha(this.senha);
			funcionario.setEmail(this.email);
			funcionario.setNome(this.nome);
			funcionario.setFuncao(this.funcao);
			funcionario.setCpf(this.cpf);
			funcionario.setIdade(this.idade);

			return funcionario;
		}

	}
}
