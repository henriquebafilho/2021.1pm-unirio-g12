package br.com.trabalhoPM.maven.controller.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ResponseDTO {
	@JsonProperty
	String id, codigo, mensagem;
	
	public ResponseDTO() {
		
	}

	public ResponseDTO(String id, String codigo, String mensagem) {
		super();
		this.id = id;
		this.codigo = codigo;
		this.mensagem = mensagem;
	}
	
	
}
