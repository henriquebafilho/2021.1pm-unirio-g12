package br.com.trabalhoPM.maven.controller.dto;

import java.time.LocalDate;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CartaoDeCreditoDTO {
	@JsonProperty
	String nomeTitular, numero, cvv;
	@JsonProperty
	UUID idCiclista;
	@JsonProperty
	LocalDate validade;
	
	public CartaoDeCreditoDTO() {
		
	}
	
	public CartaoDeCreditoDTO(UUID idCiclista, String nomeTitular, String numero, LocalDate validade, String cvv) {
		super();
		this.idCiclista = idCiclista;
		this.nomeTitular = nomeTitular;
		this.numero = numero;
		this.validade = validade;
		this.cvv = cvv;
	}
}
