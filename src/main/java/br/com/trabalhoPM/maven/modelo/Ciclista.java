package br.com.trabalhoPM.maven.modelo;

import java.time.LocalDate;
import java.util.UUID;

public class Ciclista {
	private UUID idCiclista;
	private String nome;
	private LocalDate nascimento;
	private String cpf;
	private String nacionalidade;
	private String email;
	private String senha;
	private Passaporte passaporte;
	
	@Override
	public String toString() {
		return "Ciclista [idCiclista=" + idCiclista + ", nome=" + nome + ", nascimento=" + nascimento + ", cpf=" + cpf
				+ ", nacionalidade=" + nacionalidade + ", email=" + email + ", senha=" + senha + ", passaporte="
				+ passaporte + "]";
	}

	public static class Passaporte{
		private String numero;
		private LocalDate validade;
		private String pais;
		
		@Override
		public String toString() {
			return "Passaporte [numero=" + numero + ", validade=" + validade + ", pais=" + pais + "]";
		}
		
		public String getNumero() {
			return numero;
		}
		public void setNumero(String numero) {
			this.numero = numero;
		}
		public LocalDate getValidade() {
			return validade;
		}
		public void setValidade(LocalDate validade) {
			this.validade = validade;
		}
		public String getPais() {
			return pais;
		}
		public void setPais(String pais) {
			this.pais = pais;
		}
	}
	
	public UUID getIdCiclista() {
		return idCiclista;
	}

	public void setIdCiclista(UUID idCiclista) {
		this.idCiclista = idCiclista;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public LocalDate getNascimento() {
		return nascimento;
	}

	public void setNascimento(LocalDate nascimento) {
		this.nascimento = nascimento;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getNacionalidade() {
		return nacionalidade;
	}

	public void setNacionalidade(String nacionalidade) {
		this.nacionalidade = nacionalidade;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public Passaporte getPassaporte() {
		return passaporte;
	}

	public void setPassaporte(Passaporte passaporte) {
		this.passaporte = passaporte;
	}
	
}
