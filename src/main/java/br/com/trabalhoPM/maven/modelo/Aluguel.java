package br.com.trabalhoPM.maven.modelo;

import java.util.Date;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Aluguel {

	private UUID bicicleta;
	private UUID ciclista;
	private UUID cobranca;
	private UUID trancaInicio;
	private UUID trancaFim;
	private Date horaInicio;
	private Date horaFim;
	
	@Override
	public String toString() {
		return "Aluguel [bicicleta= " + bicicleta + ", horaInicio= " + horaInicio + ", trancaFim= " + trancaFim +
				", horaFim= " + horaFim + ", cobranca= " + cobranca +
				", ciclista= " + ciclista + ", trancaInicio= " + trancaInicio + "]";
	}

	public UUID getBicicleta() {
		return bicicleta;
	}

	public void setBicicleta(UUID bicicleta) {
		this.bicicleta = bicicleta;
	}

	public UUID getCiclista() {
		return ciclista;
	}

	public void setCiclista(UUID ciclista) {
		this.ciclista = ciclista;
	}

	public UUID getCobranca() {
		return cobranca;
	}

	public void setCobranca(UUID cobranca) {
		this.cobranca = cobranca;
	}

	public UUID getTrancaInicio() {
		return trancaInicio;
	}

	public void setTrancaInicio(UUID trancaInicio) {
		this.trancaInicio = trancaInicio;
	}

	public UUID getTrancaFim() {
		return trancaFim;
	}

	public void setTrancaFim(UUID trancaFim) {
		this.trancaFim = trancaFim;
	}

	public Date getHoraInicio() {
		return horaInicio;
	}

	public void setHoraInicio(Date horaInicio) {
		this.horaInicio = horaInicio;
	}

	public Date getHoraFim() {
		return horaFim;
	}

	public void setHoraFim(Date horaFim) {
		this.horaFim = horaFim;
	}
	
}
