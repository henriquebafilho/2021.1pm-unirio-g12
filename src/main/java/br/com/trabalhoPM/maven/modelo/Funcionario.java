package br.com.trabalhoPM.maven.modelo;

import java.util.UUID;

public class Funcionario {
	private UUID idFuncionario;
	private String matricula;
	private String senha;
	private String email;
	private String nome;
	private int idade;
	private String funcao;
	private String cpf;
	
	public UUID getIdFuncionario() {
		return idFuncionario;
	}
	public void setIdFuncionario(UUID idFuncionario) {
		this.idFuncionario = idFuncionario;
	}
	public String getMatricula() {
		return matricula;
	}
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public int getIdade() {
		return idade;
	}
	public void setIdade(int idade) {
		this.idade = idade;
	}
	public String getFuncao() {
		return funcao;
	}
	public void setFuncao(String funcao) {
		this.funcao = funcao;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
}
