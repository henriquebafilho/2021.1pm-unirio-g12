package br.com.trabalhoPM.maven;

import static io.javalin.apibuilder.ApiBuilder.delete;
import static io.javalin.apibuilder.ApiBuilder.get;
import static io.javalin.apibuilder.ApiBuilder.path;
import static io.javalin.apibuilder.ApiBuilder.post;
import static io.javalin.apibuilder.ApiBuilder.put;

import br.com.trabalhoPM.maven.controller.CiclistaController;
import br.com.trabalhoPM.maven.controller.FuncionarioController;
import br.com.trabalhoPM.maven.controller.CartaoDeCreditoController;
import br.com.trabalhoPM.maven.controller.AluguelController;

import io.javalin.Javalin;

public class Main {
	public static void main(String[] args) {
        Javalin app = Javalin.create().routes(() -> {
        	path("ciclista", () -> {
        		post(CiclistaController::cadastrarCiclista);
        		path(":idCiclista", () -> {
        			get(CiclistaController::getCiclista);
        			put(CiclistaController::putCiclista);
        		});
        		path(":idCiclista/ativar", () -> {
        			get(CiclistaController::getCiclista);
        		});
        		path("/existeEmail/", () -> {
        			get(CiclistaController::getCiclista);
        		});
        		path("/existeEmail/:email", () -> {
        			get(CiclistaController::getCiclista);
        		});
        	});
        	path("funcionario", () -> {
        		get(FuncionarioController::getAllFuncionarios);
        		post(FuncionarioController::cadastrarFuncionario);
        		path(":idFuncionario", () -> {
        			get(FuncionarioController::getFuncionario);
        			put(FuncionarioController::putFuncionario);
        			delete(FuncionarioController::deleteFuncionario);
        		});
        	});
        	path("cartaoDeCredito", () -> {
        		path(":idCiclista", () -> {
        			get(CartaoDeCreditoController::getCartaoDeCredito);
        		});
        	});
        	path("aluguel", () -> {
        		post(AluguelController::aluguel);
        	});
        });
        app.start(getHerokuAssignedPort());
    }

	private static int getHerokuAssignedPort() {
		String herokuPort = System.getenv("PORT");
		if (herokuPort != null) {
			return Integer.parseInt(herokuPort);
		}
		return 7000;
	}

}
