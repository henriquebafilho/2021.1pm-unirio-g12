package br.com.trabalhoPM.maven;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;
import java.util.UUID;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import br.com.trabalhoPM.maven.controller.CiclistaController.CiclistaDTO;
import br.com.trabalhoPM.maven.controller.CiclistaController.CiclistaDTO.PassaporteDTO;
import br.com.trabalhoPM.maven.modelo.Ciclista;

class CiclistaDTOTest {

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}
	
	@Test
	void CiclistaParaCiclistaDTO() {
		Ciclista ciclista = new Ciclista();
		ciclista.setIdCiclista(UUID.fromString("b06b78ca-872d-4570-9c24-0cda1fd93a79"));
		ciclista.setNome("henrique");
		ciclista.setNascimento(LocalDate.parse("2021-09-06"));
		ciclista.setCpf("12345678");
		ciclista.setNacionalidade("brasileiro");
		ciclista.setEmail("henrique@email.com");
		ciclista.setSenha("henrique123");
		ciclista.setPassaporte(new Ciclista.Passaporte());
		ciclista.getPassaporte().setNumero("06092021");
		ciclista.getPassaporte().setPais("brasil");
		ciclista.getPassaporte().setValidade(LocalDate.parse("2100-09-06"));
		
		CiclistaDTO ciclistaDTO = new CiclistaDTO(ciclista);
		assertEquals("b06b78ca-872d-4570-9c24-0cda1fd93a79", ciclistaDTO.id);
		assertEquals("henrique", ciclistaDTO.nome);
		assertEquals("2021-09-06", ciclistaDTO.nascimento);
		assertEquals("12345678", ciclistaDTO.cpf);
		assertEquals("brasileiro", ciclistaDTO.nacionalidade);
		assertEquals("henrique@email.com", ciclistaDTO.email);
		assertEquals("henrique123", ciclistaDTO.senha);
		assertNotNull(ciclistaDTO.passaporte);
	}
	
	@Test
	void CiclistaParaCiclistaDTOSemPassaporte() {
		Ciclista ciclista = new Ciclista();
		ciclista.setIdCiclista(UUID.fromString("b06b78ca-872d-4570-9c24-0cda1fd93a79"));
		ciclista.setNome("henrique");
		ciclista.setNascimento(LocalDate.parse("2021-09-06"));
		ciclista.setCpf("12345678");
		ciclista.setNacionalidade("brasileiro");
		ciclista.setEmail("henrique@email.com");
		ciclista.setSenha("henrique123");
		
		CiclistaDTO ciclistaDTO = new CiclistaDTO(ciclista);
		assertEquals("b06b78ca-872d-4570-9c24-0cda1fd93a79", ciclistaDTO.id);
		assertEquals("henrique", ciclistaDTO.nome);
		assertEquals("2021-09-06", ciclistaDTO.nascimento);
		assertEquals("12345678", ciclistaDTO.cpf);
		assertEquals("brasileiro", ciclistaDTO.nacionalidade);
		assertEquals("henrique@email.com", ciclistaDTO.email);
		assertEquals("henrique123", ciclistaDTO.senha);
		assertNull(ciclistaDTO.passaporte);
	}
	
	@Test
	void CiclistaParaCiclistaDTOSemId() {
		Ciclista ciclista = new Ciclista();
		ciclista.setNome("henrique");
		ciclista.setNascimento(LocalDate.parse("2021-09-06"));
		ciclista.setCpf("12345678");
		ciclista.setNacionalidade("brasileiro");
		ciclista.setEmail("henrique@email.com");
		ciclista.setSenha("henrique123");
		ciclista.setPassaporte(new Ciclista.Passaporte());
		ciclista.getPassaporte().setNumero("06092021");
		ciclista.getPassaporte().setPais("brasil");
		ciclista.getPassaporte().setValidade(LocalDate.parse("2100-09-06"));
		
		CiclistaDTO ciclistaDTO = new CiclistaDTO(ciclista);
		assertNull(ciclistaDTO.id);
		assertEquals("henrique", ciclistaDTO.nome);
		assertEquals("2021-09-06", ciclistaDTO.nascimento);
		assertEquals("12345678", ciclistaDTO.cpf);
		assertEquals("brasileiro", ciclistaDTO.nacionalidade);
		assertEquals("henrique@email.com", ciclistaDTO.email);
		assertEquals("henrique123", ciclistaDTO.senha);
		assertNotNull(ciclistaDTO.passaporte);
	}
	
	@Test
	void CiclistaDTOparaCiclista() {
		CiclistaDTO ciclistaDTO = new CiclistaDTO();
		ciclistaDTO.id = "b06b78ca-872d-4570-9c24-0cda1fd93a79";
		ciclistaDTO.nome = "henrique";
		ciclistaDTO.nascimento = "2021-09-06";
		ciclistaDTO.cpf = "12345678";
		ciclistaDTO.nacionalidade = "brasileiro";
		ciclistaDTO.email = "henrique@email.com";
		ciclistaDTO.senha = "henrique123";
		ciclistaDTO.passaporte = new PassaporteDTO();
		ciclistaDTO.passaporte.numero = "06092021";
		ciclistaDTO.passaporte.pais = "brasil";
		ciclistaDTO.passaporte.validade = "2100-09-06";
		
		Ciclista ciclista = ciclistaDTO.obterCiclista();
		assertEquals(UUID.fromString("b06b78ca-872d-4570-9c24-0cda1fd93a79"), ciclista.getIdCiclista());
		assertEquals("henrique", ciclista.getNome());
		assertEquals(LocalDate.parse("2021-09-06"), ciclista.getNascimento());
		assertEquals("12345678", ciclista.getCpf());
		assertEquals("brasileiro", ciclista.getNacionalidade());
		assertEquals("henrique@email.com", ciclista.getEmail());
		assertEquals("henrique123", ciclista.getSenha());
		assertEquals("06092021", ciclista.getPassaporte().getNumero());
		assertEquals("brasil", ciclista.getPassaporte().getPais());
		assertEquals(LocalDate.parse("2100-09-06"), ciclista.getPassaporte().getValidade());
	}
	
	@Test
	void CiclistaDTOparaCiclistaSemId() {
		CiclistaDTO ciclistaDTO = new CiclistaDTO();
		ciclistaDTO.nome = "henrique";
		ciclistaDTO.nascimento = "2021-09-06";
		ciclistaDTO.cpf = "12345678";
		ciclistaDTO.nacionalidade = "brasileiro";
		ciclistaDTO.email = "henrique@email.com";
		ciclistaDTO.senha = "henrique123";
		ciclistaDTO.passaporte = new PassaporteDTO();
		ciclistaDTO.passaporte.numero = "06092021";
		ciclistaDTO.passaporte.pais = "brasil";
		ciclistaDTO.passaporte.validade = "2100-09-06";
		
		Ciclista ciclista = ciclistaDTO.obterCiclista();
		assertEquals("henrique", ciclista.getNome());
		assertEquals(LocalDate.parse("2021-09-06"), ciclista.getNascimento());
		assertEquals("12345678", ciclista.getCpf());
		assertEquals("brasileiro", ciclista.getNacionalidade());
		assertEquals("henrique@email.com", ciclista.getEmail());
		assertEquals("henrique123", ciclista.getSenha());
		assertEquals("06092021", ciclista.getPassaporte().getNumero());
		assertEquals("brasil", ciclista.getPassaporte().getPais());
		assertEquals(LocalDate.parse("2100-09-06"), ciclista.getPassaporte().getValidade());
	}
	
	@Test
	void CiclistaDTOparaCiclistaSemPassaporte() {
		CiclistaDTO ciclistaDTO = new CiclistaDTO();
		ciclistaDTO.id = "b06b78ca-872d-4570-9c24-0cda1fd93a79";
		ciclistaDTO.nome = "henrique";
		ciclistaDTO.nascimento = "2021-09-06";
		ciclistaDTO.cpf = "12345678";
		ciclistaDTO.nacionalidade = "brasileiro";
		ciclistaDTO.email = "henrique@email.com";
		ciclistaDTO.senha = "henrique123";
		
		Ciclista ciclista = ciclistaDTO.obterCiclista();
		assertEquals(UUID.fromString("b06b78ca-872d-4570-9c24-0cda1fd93a79"), ciclista.getIdCiclista());
		assertEquals("henrique", ciclista.getNome());
		assertEquals(LocalDate.parse("2021-09-06"), ciclista.getNascimento());
		assertEquals("12345678", ciclista.getCpf());
		assertEquals("brasileiro", ciclista.getNacionalidade());
		assertEquals("henrique@email.com", ciclista.getEmail());
		assertEquals("henrique123", ciclista.getSenha());
	}

}
