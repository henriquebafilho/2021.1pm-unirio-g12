package br.com.trabalhoPM.maven;

import static org.junit.jupiter.api.Assertions.*;

import java.util.UUID;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import br.com.trabalhoPM.maven.controller.FuncionarioController.FuncionarioDTO;
import br.com.trabalhoPM.maven.modelo.Funcionario;

class FuncionarioDTOTest {

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}
	
	@Test
	void FuncionarioParaFunctionarioDTO() {
		Funcionario funcionario = new Funcionario();
		funcionario.setIdFuncionario(UUID.fromString("b06b78ca-872d-4570-9c24-0cda1fd93a79"));
		funcionario.setMatricula("2021");
		funcionario.setSenha("henrique123");
		funcionario.setEmail("henrique@email.com");
		funcionario.setNome("henrique");
		funcionario.setIdade(22);
		funcionario.setFuncao("ciclista");
		funcionario.setCpf("12345678");
		
		FuncionarioDTO funcionarioDTO = new FuncionarioDTO(funcionario);
		assertEquals("b06b78ca-872d-4570-9c24-0cda1fd93a79", funcionarioDTO.id);
		assertEquals("2021", funcionarioDTO.matricula);
		assertEquals("henrique123", funcionarioDTO.senha);
		assertEquals("henrique@email.com", funcionarioDTO.email);
		assertEquals("henrique", funcionarioDTO.nome);
		assertEquals(22, funcionarioDTO.idade);
		assertEquals("ciclista", funcionarioDTO.funcao);
		assertEquals("12345678", funcionarioDTO.cpf);
	}
	
	@Test
	void FuncionarioDTOparaFuncionario() {
		FuncionarioDTO funcionarioDTO = new FuncionarioDTO();
		funcionarioDTO.id = "b06b78ca-872d-4570-9c24-0cda1fd93a79";
		funcionarioDTO.matricula = "2021";
		funcionarioDTO.senha = "henrique123";
		funcionarioDTO.email = "henrique@email.com";
		funcionarioDTO.nome = "henrique";
		funcionarioDTO.idade = 22;
		funcionarioDTO.funcao = "ciclista";
		funcionarioDTO.cpf = "12345678";
		
		Funcionario funcionario = funcionarioDTO.obterFuncionario();
		assertEquals(UUID.fromString("b06b78ca-872d-4570-9c24-0cda1fd93a79"), funcionario.getIdFuncionario());
		assertEquals("2021", funcionario.getMatricula());
		assertEquals("henrique123", funcionario.getSenha());
		assertEquals("henrique@email.com", funcionario.getEmail());
		assertEquals("henrique", funcionario.getNome());
		assertEquals(22, funcionario.getIdade());
		assertEquals("ciclista", funcionario.getFuncao());
		assertEquals("12345678", funcionario.getCpf());
	}

	@Test
	void FuncionarioDTOparaFuncionarioSemId() {
		FuncionarioDTO funcionarioDTO = new FuncionarioDTO();
		funcionarioDTO.matricula = "2021";
		funcionarioDTO.senha = "henrique123";
		funcionarioDTO.email = "henrique@email.com";
		funcionarioDTO.nome = "henrique";
		funcionarioDTO.idade = 22;
		funcionarioDTO.funcao = "ciclista";
		funcionarioDTO.cpf = "12345678";
		
		Funcionario funcionario = funcionarioDTO.obterFuncionario();
		assertNull(funcionario.getIdFuncionario());
		assertEquals("2021", funcionario.getMatricula());
		assertEquals("henrique123", funcionario.getSenha());
		assertEquals("henrique@email.com", funcionario.getEmail());
		assertEquals("henrique", funcionario.getNome());
		assertEquals(22, funcionario.getIdade());
		assertEquals("ciclista", funcionario.getFuncao());
		assertEquals("12345678", funcionario.getCpf());
	}
	
}
