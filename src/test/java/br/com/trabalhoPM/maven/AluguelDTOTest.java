package br.com.trabalhoPM.maven;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;
import java.util.Date;
import java.util.UUID;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import br.com.trabalhoPM.maven.controller.AluguelController.AluguelDTO;
import br.com.trabalhoPM.maven.controller.CiclistaController.CiclistaDTO;
import br.com.trabalhoPM.maven.controller.CiclistaController.CiclistaDTO.PassaporteDTO;
import br.com.trabalhoPM.maven.modelo.Aluguel;
import br.com.trabalhoPM.maven.modelo.Ciclista;

class AluguelDTOTest {

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}
	
	@Test
	void AluguelParaAluguelDTO() {
		Aluguel aluguel = new Aluguel();
		Date horaInicio = new Date();
		Date horaFim = new Date();
		aluguel.setBicicleta(UUID.fromString("f3c9d712-6f9a-4efe-8de5-ccc6e0990ba9"));
		aluguel.setCiclista(UUID.fromString("6921a255-9698-4bff-87fb-49e5f2534c65"));
		aluguel.setCobranca(UUID.fromString("f3f0eb4a-95b3-4653-b4ec-c016b26f706e"));
		aluguel.setHoraInicio(horaFim);
		aluguel.setHoraFim(horaInicio);
		aluguel.setTrancaInicio(UUID.fromString("62d4e0d5-a4e2-4b69-82d8-95b8fc505bc6"));	
		aluguel.setTrancaFim(UUID.fromString("94b39731-0008-4e01-bd96-47fc8d8d277f"));
		
		AluguelDTO aluguelDTO = new AluguelDTO(aluguel);
		assertEquals("f3c9d712-6f9a-4efe-8de5-ccc6e0990ba9", aluguelDTO.bicicleta);
		assertEquals("6921a255-9698-4bff-87fb-49e5f2534c65", aluguelDTO.ciclista);
		assertEquals("f3f0eb4a-95b3-4653-b4ec-c016b26f706e", aluguelDTO.cobranca);
		assertEquals(horaInicio, aluguelDTO.horaInicio);
		assertEquals(horaFim, aluguelDTO.horaFim);
		assertEquals("62d4e0d5-a4e2-4b69-82d8-95b8fc505bc6", aluguelDTO.trancaInicio);
		assertEquals("94b39731-0008-4e01-bd96-47fc8d8d277f", aluguelDTO.trancaFim);
	}
	
	@Test
	void AluguelDTOparaAluguel() {
		AluguelDTO aluguelDTO = new AluguelDTO();
		Date horaInicio = new Date();
		Date horaFim = new Date();
		aluguelDTO.bicicleta = "f3c9d712-6f9a-4efe-8de5-ccc6e0990ba9";
		aluguelDTO.ciclista = "6921a255-9698-4bff-87fb-49e5f2534c65";
		aluguelDTO.cobranca = "f3f0eb4a-95b3-4653-b4ec-c016b26f706e";
		aluguelDTO.horaInicio = horaInicio;
		aluguelDTO.horaFim = horaFim;
		aluguelDTO.trancaInicio = "62d4e0d5-a4e2-4b69-82d8-95b8fc505bc6";
		aluguelDTO.trancaFim = "94b39731-0008-4e01-bd96-47fc8d8d277f";
		
		Aluguel aluguel = aluguelDTO.obterAluguel();
		assertEquals(UUID.fromString("f3c9d712-6f9a-4efe-8de5-ccc6e0990ba9"), aluguel.getBicicleta());
		assertEquals(UUID.fromString("6921a255-9698-4bff-87fb-49e5f2534c65"), aluguel.getCiclista());
		assertEquals(UUID.fromString("f3f0eb4a-95b3-4653-b4ec-c016b26f706e"), aluguel.getCobranca());
		assertEquals(horaInicio, aluguel.getHoraInicio());
		assertEquals(horaFim, aluguel.getHoraFim());
		assertEquals(UUID.fromString("62d4e0d5-a4e2-4b69-82d8-95b8fc505bc6"), aluguel.getTrancaInicio());
		assertEquals(UUID.fromString("94b39731-0008-4e01-bd96-47fc8d8d277f"), aluguel.getTrancaFim());
	}

}
