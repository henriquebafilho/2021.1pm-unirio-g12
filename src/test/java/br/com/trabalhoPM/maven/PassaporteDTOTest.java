package br.com.trabalhoPM.maven;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import br.com.trabalhoPM.maven.controller.CiclistaController.CiclistaDTO.PassaporteDTO;
import br.com.trabalhoPM.maven.modelo.Ciclista.Passaporte;

class PassaporteDTOTest {

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}
	
	@Test
	void PassaporteParaPassaporteDTO() {
		Passaporte passaporte = new Passaporte();
		passaporte.setNumero("06092021");
		passaporte.setPais("Brasil");
		passaporte.setValidade(LocalDate.parse("2021-09-06"));
		
		PassaporteDTO passaporteDTO = new PassaporteDTO(passaporte);
		assertEquals("06092021", passaporteDTO.numero);
		assertEquals("Brasil", passaporteDTO.pais);
		assertEquals("2021-09-06", passaporteDTO.validade);
	}
	
	@Test
	void PassaporteDTOparaPassaporte() {
		PassaporteDTO passaporteDTO = new PassaporteDTO();
		passaporteDTO.numero = "06092021";
		passaporteDTO.pais = "Brasil";
		passaporteDTO.validade = "2021-09-06";
		
		Passaporte passaporte = passaporteDTO.obterPassaporte();
		assertEquals("06092021", passaporte.getNumero());
		assertEquals("Brasil", passaporte.getPais());
		assertEquals(LocalDate.parse("2021-09-06"), passaporte.getValidade());
		
	}

}
